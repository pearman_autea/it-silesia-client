# **Client** - simple client for testing

In chrome console :
~~~~
connect('localhost:4000'); // establish websocket connection to service
emit(event, data); // send websocket event to service - equals to socket.emit(event, data)
~~~~

# **Basic flow**

## Authentication
Just after establishing websocket connection the client has to authenticate itself within 1s.
~~~~
// TOTEM AUTHENTICATION
const data = {
  username: "test",
  password: "halo"
}
socket.emit('authentication', data) 
~~~~
~~~~
// DEVICE AUTHENTICATION
const data = {
  username: "device",
  password: "device"
}
socket.emit('authentication', data) 
~~~~

### Client receives event *authenticated* if credentials are valid or *unathorized* if they are not :
~~~~
socket.on("authenticated", (data) => { 
  // ...   
});
~~~~
~~~~
socket.on("unathorized", (data) => { 
  data === 'Unauthorized.'   
});
~~~~
~~~~
socket.on("authentication-error", (err) => { 
  const {status, message} = err; // message is optional !
});
~~~~

## Totem connects to service  
~~~~
const data = { 
  totemId: "1234567890abcdefghijklmn" // proper totemId has 24 chars, totem must be registered in totem-management service
}
socket.emit("totem-connect", data);
~~~~
    
### Totem receives event *totem-connected* with data : 
~~~~
socket.on("totem-connected", (data) => { 
  const { totemId } = data;   
});
~~~~
~~~~
socket.on("totem-connect-error", (err) => { 
  const {status, message, data} = err; // data property is optional
});
~~~~

jsonSchema : https://bitbucket.org/tapchan/totem-core-interactions/src/90c170b3fb66a372c6b77f2986d55bd0af137878/schemas/totem-connect.json?at=stage  
    
## Totem asks for connection hash
~~~~
// all values are optional, default values presented
const data = {
  length : 3, 
  sessionsLimit : 1, // it is overwritten in backend for now - set to 1
  reconnect : false  // defines if connected device should get new hash 
}
socket.emit("connection-hash-request", data);
~~~~ 

jsonSchema : https://bitbucket.org/tapchan/totem-core-interactions/src/90c170b3fb66a372c6b77f2986d55bd0af137878/schemas/connection-hash-request.json?at=stage  
    
### Totem receives event *connection-hash-created* with data : 
~~~~ 
socket.on("connection-hash-created", (data) => { 
  const { hash } = data;   
}); 
~~~~
~~~~
socket.on("connection-hash-request-error", (err) => { 
  const {status, message} = err;
});
~~~~ 
  
## Device connects to service using connection hash from totem 
~~~~
const data = { 
  deviceId : "0987654321abcdefghijklmn", 
  domain : "stage.sample.com", 
  hash : "abc"
}
socket.emit("device-connect", data);
~~~~

jsonSchema : https://bitbucket.org/tapchan/totem-core-interactions/src/90c170b3fb66a372c6b77f2986d55bd0af137878/schemas/device-connect.json?at=stage  

### Device receives event *device-connected* with data : 
~~~~
socket.on("device-connected", (data) => { 
  const { deviceId, mobileContentUrl } = data;   
});
~~~~
~~~~
socket.on("device-connect-error", (err) => { 
  const {status, message} = err;
});
~~~~ 

### Totem receives event *device-connected* with data : 
~~~~
socket.on("device-connected", (data) => { 
  const { sessionId, source } = data;   
  // source -> deviceId
});
~~~~

## Totem can send *message* to device :
~~~~
const message =  { 
  data: {}, 
  event: "click", 
  destination: "abcdefghijklmnoprstq1234.abc.0987654321abcdefghijklmn"
}
socket.emit("message", message);
~~~~
~~~~
socket.on("message-error", (err) => { 
  const {status, message} = err;
});
~~~~ 

jsonSchema : https://bitbucket.org/tapchan/totem-core-interactions/src/90c170b3fb66a372c6b77f2986d55bd0af137878/schemas/totem-message.json?at=stage  

### Device receives event *message* with data : 
~~~~
socket.on("message", (message) => { 
  const { data, event } = message;   
});
~~~~ 
    
## Device can send *message* to totem :
~~~~
const message =  { 
  data: {}, 
  event: "click"
}
socket.emit("message", message);
~~~~
~~~~
socket.on("message-error", (err) => { 
  const {status, message} = err;
});
~~~~ 

jsonSchema : https://bitbucket.org/tapchan/totem-core-interactions/src/90c170b3fb66a372c6b77f2986d55bd0af137878/schemas/device-message.json?at=stage  

### Totem receives event *message* with data : 
~~~~
socket.on("message", (message) => { 
  const { data, event, sessionId, source } = message;   
});
~~~~

## CAUTION !!!
  data in emit() function must be an object
  example :
~~~~
emit('message', { 
  event: "click", 
  data: { count: 1 } 
})
~~~~

## Device can receive *reconnect-hash* :
~~~~
socket.on("reconnect-hash", (data) => { 
  const { hash } = data;   
});
~~~~

## Totem & Device can measure latency :
### To measure latency *latency-start* event must be sent
~~~~
const message =  { 
  anomaly: 100 // defines latency value that is considered as too high // optional - if provided anomalies would be measured 
}
socket.emit("latency-start", message);
~~~~

jsonSchema : https://bitbucket.org/tapchan/totem-core-interactions/src/90c170b3fb66a372c6b77f2986d55bd0af137878/schemas/latency-start.json?at=stage  

### To stop measure latency *latency-stop* event must be sent
~~~~
socket.emit("latency-stop");
~~~~

### To get latency measurement results *latency-ack* event must be sent with data received from event *latency-test* 
~~~~
socket.on("latency-test", (data) => { 
  socket.emit("latency-ack", data);   
});
~~~~

### Latency measurement results are sent with *latency-totem* and *latency-device* events
~~~~
socket.on("latency-totem", (data) => { 
  const { latency } = data;   
});

socket.on("latency-device", (data) => { 
  const { latency } = data;   
});
~~~~

### Detected latency anomalies are sent with *latency-totem-anomaly* and *latency-device-anomaly* events
~~~~
socket.on("latency-totem-anomaly", (data) => { 
  const { latency } = data;   
});

socket.on("latency-device-anomaly", (data) => { 
  const { latency } = data;   
});
~~~~

## Additional events
### Totem is notified about device disconnection
~~~~
socket.on("device-disconnected", (data) => { 
  const { deviceId } = data;   
});
~~~~

### Device is notified about totem disconnection
~~~~
socket.on("totem-disconnected", (data) => { 
  const { hash } = data;   
});
~~~~

### Device is notified about finished session
~~~~
socket.on("session-finished", (data) => { 
  const { hash } = data;   
});
~~~~

## Errors
Errors always contains status and message fields. Statuses refers to REST status codes. Messages can vary depending on error case. 
~~~~
const INVALID_DATA = {
    status: 400,
    message: 'Invalid data sent. Please check documentation.'
}

const UNAUTHORIZED = {
    status: 401,
    message: 'Unauthorized.'
}

const FORBIDDEN = {
    status: 403,
    message: 'Could not perform requested action.'
}

const NOT_FOUND = {
    status: 404,
    message: 'Resource not found.'
}

const TIMEOUT = {
    status: 408,
    message: 'Request timeout. Please check documentation.'
}

const CONFLICT = {
    status: 409,
    message: 'Could not handle the request.'
}

const INTERNAL_SERVER_ERROR = {
    status: 500,
    message: 'Internal server error.'
}

const SERVICE_UNAVAILABLE = {
    status: 503,
    message: 'Service is temporarily unavailable.'
}
~~~~
